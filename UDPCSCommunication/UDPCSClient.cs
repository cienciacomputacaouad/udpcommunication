﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace UDPCSCommunication
{
    class UDPCSClient
    {
        static void Main(string[] args)
        {
            byte[] data = new byte[1024];
            string stringData;
            UdpClient server = new UdpClient("127.0.0.1", 6789);

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

            string welcome = "Hello, are you there?";
            data = Encoding.ASCII.GetBytes(welcome);
            server.Send(data, data.Length);

            data = server.Receive(ref sender);

            Console.WriteLine("Message received from {0}: ", sender.ToString());
            stringData = Encoding.ASCII.GetString(data, 0, data.Length);
            Console.WriteLine(stringData);

            server.Close();

            Console.ReadKey();
        }
    }
}
