﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace UDPCSServer
{
    class UDPCSServer
    {

        private static string welcome = "Bem vindo ao servidor de teste";

        static void Main(string[] args)
        {
            int recv;
            byte[] welcomeData = Encoding.ASCII.GetBytes(welcome);

            IPEndPoint ipConnexion = new IPEndPoint(IPAddress.Any, 6789);

            Socket serverSocket = new Socket(AddressFamily.InterNetwork, 
                                                SocketType.Dgram, 
                                                ProtocolType.Udp);

            serverSocket.Bind(ipConnexion);

            Console.WriteLine("Esperando um cliente... ");

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint remote = (EndPoint)(sender);            

            while (true)
            {
                byte[] data = new byte[1024];

                recv = serverSocket.ReceiveFrom(data, ref remote);

                Console.WriteLine("Mensagem recebida de {0}: ", remote.ToString());
                Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));

                serverSocket.SendTo(welcomeData, 
                                    welcomeData.Length, 
                                    SocketFlags.None, 
                                    remote);
            }

        }
    }
}
